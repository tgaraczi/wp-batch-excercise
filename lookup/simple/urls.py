from django.conf.urls import patterns, url
from simple import views


urlpatterns = patterns('',
                       url(r'^fill/$', views.fill, name='fill'),
                       url(r'^query/$', views.query, name='query'),
                       url(r'^result/$', views.result, name='result'),
)