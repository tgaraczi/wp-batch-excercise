from django.db import models, transaction
import requests
import time
import concurrent.futures
from django.conf import settings

# Create your models here.
def fill_entry_fields(entry_id):
	entry = Entry.objects.get(id=entry_id)
	try:
		entry.fill_fields()
	except Exception as err:
		entry.set_filled()
		print(str(err))

class List(models.Model):
    id = models.AutoField(primary_key=True)
    header = models.CharField(max_length=1000, default="")
    name_field_number = models.IntegerField(default=0)
    phone_field_number = models.IntegerField(default=0)
    address_field_number = models.IntegerField(default=0)
    apikey = models.CharField(max_length=1000, default="304d26329d13ef991b471d83813131bd")

    def fill_operations_left(self):
    	result = 0
    	for entry in self.entry_set.all():
    		if not entry.filled:
    			result += 1
    	return result

    @classmethod
    def from_csv(cls, apikey, csv):
    	lines = csv.splitlines()
    	list = cls(apikey=apikey,header=lines[0])
    	list.save()
    	list.discover_headers()
    	for i in range(1,len(lines)):
    		entry = Entry(list=list,text=lines[i])
    		entry.save()
    		list.save()
    	return list

    @classmethod
    def pop(cls, id):
    	l = cls.objects.get(id=id)
    	if l.fill_operations_left() > 0:
    		return str(l.fill_operations_left())
    	else:
    		csv = l.to_csv()
    		l.delete()
    		print(csv)
    		return csv

    def fill_missing_fields(self):
    	with concurrent.futures.ThreadPoolExecutor(max_workers=50) as executor:
    		for entry in self.entry_set.all():
    			executor.submit(fill_entry_fields, entry.id)

    def to_csv(self):
    	csv = self.header
    	for entry in self.entry_set.all():
    		csv = csv + '\n' + entry.text
    	return csv

    def discover_headers(self):
    	self.name_field_number = self.field_index("name")
    	self.phone_field_number = max([
    		self.field_index("phone number"),
    		self.field_index("phonenumber"),
    		self.field_index("phone_number")])
    	self.address_field_number = self.field_index("address")
    	self.save()

    def field_index(self, field_name):
    	fields = self.header.split(",")
    	i = 0
    	for field in fields:
    		if field.lower() == field_name.lower():
    			return i
    		i = i + 1
    	return -1


class Entry(models.Model):
	id = models.AutoField(primary_key=True)
	list = models.ForeignKey(List)
	filled = models.BooleanField(default=False)
	text = models.CharField(max_length=1000, default="")

	def get_fields(self):
		result = []
		field_candidates = self.text.split('"')
		for i in range(0,len(field_candidates)):
			if i % 2 == 0:
				# out of quotes
				fields = field_candidates[i].split(",")
				for f in fields:
					if len(f) > 0:
						result.append(f.strip())
			else:
				# in quotes, no further split
				f = field_candidates[i]
				result.append(f.strip())
			i = i + 1
		return result

	def get_name(self):
		fields = self.get_fields()
		return fields[self.list.name_field_number]

	def get_phone(self):
		fields = self.get_fields()
		return fields[self.list.phone_field_number]

	def get_address(self):
		fields = self.get_fields()
		return fields[self.list.address_field_number]

	def is_field_empty(self, index):
		fields = self.get_fields()
		return len(fields[index])==0

	def set_fields(self, name, phone, address):
		fields = self.get_fields()
		fields[self.list.name_field_number] = name
		fields[self.list.phone_field_number] = phone
		fields[self.list.address_field_number] = address
		result = ''
		for f in fields:
			if f is not None:
				result = result + ','
				if f.find(',') > -1:
					result = result + '"' + f + '"'
				else:
					result = result + f
		self.text = result[1:]
		self.save()
		self.set_filled()

	def set_filled(self):
		self.filled = True
		self.save()
		return

	# TODO
	# TESTS
	# TESTS
	# TESTS

	def fill_fields(self):
		# TODO: test negative indices
		fields = self.get_fields()
		name = self.get_name()
		phone = self.get_phone()
		address = self.get_address()
		if len(phone) > 0:
			reverse_phone = self.reverse_phone(self.list.apikey,phone)
			print(reverse_phone)
			name = reverse_phone['name']
			address = reverse_phone['address']
			self.set_fields(name, phone, address)
			return
		if len(name) > 0:
			reverse_name = self.reverse_name(self.list.apikey,name)
			print(reverse_name)
			address = reverse_name['address']
			phone = reverse_name['phone']
			self.set_fields(name, phone, address)
			return
		if len(address) > 0:
			reverse_address = self.reverse_address(self.list.apikey,address)
			phone = reverse_address['phone']
			name = reverse_address['name']
			self.set_fields(name, phone, address)
			return

	@staticmethod
	def get_most_valid_user(users):
		validity = -1
		most_valid_user = None
		if len(users) > 0:
			for u in users:
				if u["valid_for"] is not None:
					if u["valid_for"]["stop"] is None:
						return u
					if validity < u["valid_for"]["stop"]:
						validity = u["valid_for"]["stop"]
						most_valid_user = u
			if most_valid_user is None:
				most_valid_user = users[0]
		return most_valid_user

	@staticmethod
	def extract_address_from_user(u):
		address = ""
		if 'locations' in u.keys() and len([u['locations']]) > 0:
			try:
				l = u['locations'][0]
				if 'address' in l.keys():
					address = l['address']
			except Exception:
				pass
		if len(address) < 1:
			if 'best_location' in u.keys():
				bl = 'best_location'
				if bl is not None:
					if 'address' in bl:
						address = bl['address']
		return address

	@staticmethod
	def extract_name_from_user(u):
		if 'name' in u.keys():
			# business
			name = u["name"]
		else:
			# person
			name = u["best_name"]
		return name

	@staticmethod
	def extract_phone_from_user(u):
		if 'phones' in u.keys():
			phones = u["phones"]
			if phones is not None and len(phones) > 0:
				phone = phones[0]
				if phone is not None:
					if 'phone_number' in phone.keys():
						n = phone["phone_number"]
						if n is not None:
							return n
		return ""
		


	@staticmethod
	def reverse_phone(apikey, phone):
		url = "https://proapi.whitepages.com/2.1/phone.json"
		data = {
			'api_key': apikey,
			'phone_number': phone
			}
		reply = requests.get(url, params=data)
		j = reply.json()
		users = j["results"][0]["belongs_to"]
		user = Entry.get_most_valid_user(users)
		if user is not None:
			name = Entry.extract_name_from_user(user)
			address = Entry.extract_address_from_user(user)
		if address is None or len(address)<1:
			address = Entry.extract_address_from_user(j["results"][0])
		return {'name': name, 'address': address}

	@staticmethod
	def reverse_name(apikey, name):
		url = "https://proapi.whitepages.com/2.1/person.json"
		data = {
			'api_key': apikey,
			'name': name,
			'page_first': '0',
			'page_len': '3'
			}
		reply = requests.get(url, params=data)
		j = reply.json()
		users = j["results"]
		if len(users) > 0:
			user = users[0]
			address = Entry.extract_address_from_user(user)
			phone = Entry.extract_phone_from_user(user)
		return {'phone': phone, 'address': address}

	@staticmethod
	def reverse_address(apikey, address):
		url = "https://proapi.whitepages.com/2.1/location.json"
		data = {
			'api_key': apikey,
			'address': address,
			'page_first': '0',
			'page_len': '3'
			}
		reply = requests.get(url, params=data)
		j = reply.json()
		try:
			user = j["results"][0]["legal_entities_at"][0]
			name = Entry.extract_name_from_user(user)
			phone = Entry.extract_phone_from_user(user)
		except Exception as err:
			print(str(err))
			return {'phone': '', 'name': ''}
		return {'phone': phone, 'name': name}
