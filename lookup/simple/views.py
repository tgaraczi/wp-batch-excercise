from django.shortcuts import render
from rest_framework import status
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from simple.models import *
from django.http import HttpResponse
import threading
import urllib.request
import urllib.parse

def handle_filling(l):
	l.fill_missing_fields()

@csrf_exempt
def fill(request):
	try:
		api_key = request.POST['apikey']
		id = request.POST['id']
		l = List.objects.get(id=id)
		l.fill_missing_fields()
		return HttpResponse(str(l.id))
	except Exception as err:
		print(str(err))
		return HttpResponse({'bad request': 'bad request'}, status = status.HTTP_400_BAD_REQUEST)

@csrf_exempt
def query(request):
	try:
		api_key = request.POST['apikey']
		csv = request.POST['csv']
		l = List.from_csv(api_key, csv)
		t = threading.Thread(target=handle_filling, args=(l,))
		t.daemon = False
		t.start()
		return HttpResponse(str(l.id))
	except Exception as err:
		print(str(err))
		return HttpResponse({'bad request': 'bad request'}, status = status.HTTP_400_BAD_REQUEST)

@csrf_exempt
def result(request):
	try:
		apikey = request.POST['apikey']
		id = request.POST['id']
		return HttpResponse(List.pop(id))
	except Exception as err:
		print(str(err))
		return HttpResponse({'bad request': 'bad request'}, status = status.HTTP_400_BAD_REQUEST)