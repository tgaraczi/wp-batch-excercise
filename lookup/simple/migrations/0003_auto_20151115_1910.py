# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('simple', '0002_auto_20151113_0825'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='list',
            name='fill_operations_left',
        ),
        migrations.AddField(
            model_name='entry',
            name='filled',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='list',
            name='apikey',
            field=models.CharField(default='304d26329d13ef991b471d83813131bd', max_length=1000),
            preserve_default=True,
        ),
    ]
