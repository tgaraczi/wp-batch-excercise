# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Entry',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('text', models.CharField(max_length=1000)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='List',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('header', models.CharField(max_length=1000, default='')),
                ('name_field_number', models.IntegerField(default=0)),
                ('phone_field_number', models.IntegerField(default=0)),
                ('address_field_number', models.IntegerField(default=0)),
                ('fill_operations_left', models.IntegerField(default=0)),
                ('apikey', models.CharField(max_length=1000, default='sample')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='entry',
            name='list',
            field=models.ForeignKey(to='simple.List'),
            preserve_default=True,
        ),
    ]
