# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('simple', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entry',
            name='text',
            field=models.CharField(max_length=1000, default=''),
            preserve_default=True,
        ),
    ]
